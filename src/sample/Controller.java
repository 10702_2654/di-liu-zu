package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox co2;
    public ComboBox co3;
    public ComboBox co1;
    public ComboBox co4;
    public Button bt1;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<String> area = new ArrayList<>();
        area.add("台北");
        area.add("桃園");
        area.add("台中");
        area.add("台南");
        area.add("自經區");
        co1.setItems(FXCollections.observableArrayList(area));

        List<String> movie = new ArrayList<>();
        movie.add("阿拉丁");
        movie.add("罌粟小子");
        movie.add("復聯4");
        movie.add("名偵探雷丘");
        movie.add("東京死神:柯南");
        co2.setItems(FXCollections.observableArrayList(movie));

        List<String> date = new ArrayList<>();
        date.add("2019/05/10(五)");
        date.add("2019/05/11(六)");
        date.add("2019/05/12(日)");
        date.add("2019/05/13(一)");
        date.add("2019/05/14(二)");
        co3.setItems(FXCollections.observableArrayList(date));

        List<String> time = new ArrayList<>();
        time.add("9:30");
        time.add("12:00");
        time.add("14:30");
        time.add("16:30");
        time.add("18:00");
        co4.setItems(FXCollections.observableArrayList(time));
    }
}
